from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from .models import Hat, LocationVO
from common.json import QuerySetEncoder, ModelEncoder
from django.views.decorators.http import require_http_methods
import json

# Create your views here.

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "import_href"]

class HatsListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style_name",
    ]

class HatsDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style_name",
        "fabric",
        "color",
        "picture_url",
        "location"
    ]

    encoders = {
        "location": LocationVOEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatsDetailEncoder,
            safe=False
    )
    else:
        content = json.loads(request.body)
        try:
            #try to assign variable to location href, pass into specific hat
            location_href = content["location"]
            location=LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "invalid location id"},
                status=400
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatsDetailEncoder,
            safe=False
        )

# @require_http_methods(["GET", "DELETE"])
# def api_show_hat_details(request, id):
#     if request.method == "GET":
#         hat = Hat.objects.get(id=id)
#         return JsonResponse(
#             hat,
#             HatsDetailEncoder,
#             False
#     )

@require_http_methods(["DELETE"])
def api_delete_hat(request, id):
    if request.method == "DELETE":
        count, _ = Hat.objects.filter(id=id).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
