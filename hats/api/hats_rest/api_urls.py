from django.urls import path
from .api_views import api_list_hats, api_delete_hat

urlpatterns = [
    path("hats/", api_list_hats, name="list_hats" ),
    path("hats/<int:id>",api_delete_hat ,name="api_delete_hat")
]
