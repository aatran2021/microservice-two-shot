from django.db import models
from django.urls import reverse

# Create your models here.

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True )
    closet_name = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.closet_name}"

class Hat(models.Model):
    fabric = models.CharField(max_length=40)
    style_name = models.CharField(max_length = 40)
    color = models.CharField(max_length = 40)
    picture_url = models.URLField(null=True, max_length=400)

    location = models.ForeignKey(
        LocationVO,
        related_name="hat",
        on_delete=models.PROTECT
    )

    def __str__(self):
        return f"{self.style_name}"

    def get_api_url(self):
        return reverse("api_delete_hat", kwargs={"id": self.id})
