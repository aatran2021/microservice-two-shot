# Wardrobify

Team:

* Aaron Tran - Shoes
* Michael - Hats

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

We created a shoe model that tracks its manufacturer, model name, color, a URL for a picture, and the bin that the shoe is associated to in the wardrobe.

In order to access the bins from the wardrobe, we created a poller that requests that data and creates a ShoeVO model that contains only the needed information (in our case the import_href and closet_name).

With these pieces of data we can implement view functions that can get a list of the shoes, create a new shoe, and delete a shoe.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

In the hats microservice, we created a Hat model that keeps track of its fabric, style name, color, and location in the wardrobe where it exists. The location is a foreign key to a Location model in the wardrobe microservice.

We created a poller to request the necessary location data from the wardrobe (import_href and closet_name), and a LocationVO model is subsequently created with that data. The hats microservice then uses this data in the LocationVO in its view functions to get the list of shoes, create a shoe, and delete a shoe.
