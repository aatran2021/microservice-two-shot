from django.contrib import admin
from .models import Shoes, BinVO
# Register your models here.
@admin.register(Shoes)
class ShoesAdmin(admin.ModelAdmin):
    list_display = (
        "manufacturer",
        "model_name",
        "color",
        "url",
        "bin",
    )

@admin.register(BinVO)
class BinVOAdmin(admin.ModelAdmin):
    list_display = (
        "closet_name",
    )
