from django.db import models
from django.urls import reverse

# VO should grab only required data
class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100)

# If you need access to the url from an instance of a method use get_api_url
class Shoes(models.Model):
    manufacturer = models.CharField(max_length=150)
    model_name = models.CharField(max_length=150)
    color = models.CharField(max_length=150)
    url = models.URLField(null=True, max_length= 200)
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.PROTECT,
        null=True,
    )

    def get_api_url(self):
        return reverse("api_show_shoes", kwargs={"pk": self.pk})
