import React, { useEffect, useState } from 'react';

export default function ShoeForm() {
    const [bins, setBins] = useState([])
    const [modelName, setModelName] = useState("")
    const [manufacturer, setManufacturer] = useState("")
    const [color, setColor] = useState("")
    const [imgUrl, setImgUrl] = useState("")
    const [bin, setBin] = useState("")

    const handleUpdate = function(e, stateFunction) {
        const value = e.target.value
        stateFunction(value)
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        const data = {}

        data.model_name = modelName
        data.manufacturer = manufacturer
        data.color = color
        data.url = imgUrl
        data.bin = bin

        console.log(data);

        const shoesUrl = "http://localhost:8080/api/shoes/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        }

        const response = await fetch(shoesUrl, fetchConfig)
        if (response.ok) {
            const newShoes = await response.json()
            // console.log(newShoes)

            setModelName("")
            setManufacturer("")
            setColor("")
            setImgUrl("")
            setBin("")
        }
    }

    // Remember to call data from correct link assoicated with that data
    const fetchData = async() => {
        const url = "http://localhost:8100/api/bins/"
        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            setBins(data.bins)
            // console.log(data.bins)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Adding a New Shoe</h1>
                    <form id="create-location-form" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input onChange={(e) => handleUpdate(e, setModelName)} value={modelName} placeholder="Model Name" required type="text" name="name" id="name" className="form-control"/>
                            <label htmlFor="name">Model Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={(e) => handleUpdate(e, setManufacturer)} value={manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                            <label htmlFor="manufacturer">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={(e) => handleUpdate(e, setColor)} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={(e) => handleUpdate(e, setImgUrl)} value={imgUrl} placeholder="Image Url" required type="text" name="url" id="url" className="form-control"/>
                            <label htmlFor="url">Image Url</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={(e) => handleUpdate(e, setBin)} value={bin} required name="bin" id="bin" className="form-select">
                                <option value="">Choose a Bin</option>
                                {bins.map(bin => {
                                    return (
                                        <option value={bin.href} key={bin.href}>
                                            {bin.closet_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
