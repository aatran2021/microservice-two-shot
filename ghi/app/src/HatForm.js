import React, { useEffect, useState } from 'react'

function HatForm() {
    const [locations, setLocations] = useState([])
    const [styleName, setStyleName] = useState('')
    const [fabric, setFabric] = useState('')
    const [color, setColor] = useState('')
    const [pictureUrl, setPictureUrl] = useState('')
    const [location, setLocation] = useState('')

    const handleStyleNameChange = (event) => {
        const value = event.target.value;
        setStyleName(value)
    }
    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value)
    }
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value)
    }
    const handlePictureUrlChange = (event) => {
        const value = event.target.value
        setPictureUrl(value)
    }

    const handleLocationChange = (event) => {
        const value = event.target.value
        setLocation(value)
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        const data = {}

        data.style_name = styleName
        data.fabric = fabric
        data.color = color
        data.picture_url = pictureUrl
        data.location = location
        // console.log(data)

        const hatsUrl = "http://localhost:8090/api/hats/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            }
        }
        const response = await fetch(hatsUrl, fetchConfig)
        console.log(response)
        if (response.ok) {
            const newHat = await response.json()
            // console.log(newHat)

            setStyleName("")
            setFabric("")
            setColor("")
            setPictureUrl("")
            setLocation("")
        }
    }

    const fetchData = async() => {
        const locationsUrl = "http://localhost:8100/api/locations"
        const response = await fetch(locationsUrl)

        if (response.ok) {
            const data = await response.json()
            console.log(data)
            setLocations(data.locations)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Adding a New Hat</h1>
                    <form id="create-hat-form" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input onChange={handleStyleNameChange} value={styleName} placeholder="Style Name" required type="text" name="name" id="name" className="form-control"/>
                            <label htmlFor="name">Style Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFabricChange} value={fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control"/>
                            <label htmlFor="manufacturer">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePictureUrlChange} value={pictureUrl} placeholder="Picture Url" required type="text" name="url" id="url" className="form-control"/>
                            <label htmlFor="url">Picture Url</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleLocationChange} value={location} required name="location" id="location" className="form-select">
                                <option value="">Choose a Location</option>
                                {locations.map(location => {
                                    return (
                                        <option value={location.href} key={location.href}>
                                            {location.closet_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default HatForm
