import { Link } from 'react-router-dom'
import React, { useEffect, useState } from 'react'

function HatsList(props) {
    const [hats, setHats] = useState([])

    const handleDelete = async(value) => {
        const hatUrl = `http://localhost:8090${value}`
        const fetchConfig = {
            method: "delete",
        }
        const response = await fetch(hatUrl, fetchConfig)
        if (response.ok) {
            fetchData()
        }
    }

    const fetchData = async () => {
        const url = "http://localhost:8090/api/hats/"

        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setHats(data.hats)
            // console.log(data)
        }
    }
    useEffect(() => {
        fetchData()
    }, [])
    return (
        <>
            <div className="d-grid gap-2 d-sm-flex mt-2 mb-2">
                <Link to="/hats/new" className="btn btn-primary btn-lg px-4 gap-3">Add a Hat</Link>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Hats</th>
                        <th>Fabric</th>
                        <th>Color</th>
                        <th>Picture</th>
                        <th>Location</th>
                        <th>Delete?</th>
                    </tr>
                </thead>
                <tbody>
                    {hats.map((hat, index) => {
                        return (
                        <tr key={hat.style_name + index}>
                            <td>{ hat.style_name }</td>
                            <td>{ hat.fabric }</td>
                            <td>{ hat.color }</td>
                            <td> <img src={hat.picture_url} alt={hat.style_name} className="img-thumbnail" height="150" width="150"/></td>
                            <td>{ hat.location.closet_name }</td>
                            <td> <button className="btn btn-danger" onClick={() => handleDelete(hat.href)}>X</button></td>
                        </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    )
}

export default HatsList;
