import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'

export default function ShoesList() {
    const [shoes, setShoes] = useState([])

    // Create a way to delete individual shoes
    // Pass shoe.href through onClick event to grab shoesUrl
    // Call RESTful Api for DELETE
    // Rerender the data
    const handleDelete = async(value) => {
        const shoesUrl = `http://localhost:8080${value}`
        const fetchConfig = {
            method: "delete",
        }

        const response = await fetch(shoesUrl, fetchConfig)
        if (response.ok) {
            console.log("Delete successful")
            fetchData()
        }
    }

    // Grabs data from the url (path linked with function in api_urls.py)
    // Updates state with the data retrieved from microservice
    // Renders data via useEffect
    const fetchData = async() => {
        const url = "http://localhost:8080/api/shoes/";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json()
            setShoes(data.shoes)
            // console.log(data)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div className="my-5 container">
            <Link to="/shoes/new" className="btn btn-primary">+ New Shoes</Link>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Shoe Models</th>
                        <th>Manufacturer</th>
                        <th>Color</th>
                        <th>Image</th>
                        <th>Delete?</th>
                    </tr>
                </thead>
                <tbody>
                    {shoes.map((shoe, index) => {
                        return(
                            <tr key={shoe.model_name + index}>
                                <td> {shoe.model_name} </td>
                                <td> {shoe.manufacturer} </td>
                                <td> {shoe.color} </td>
                                <td> <img src={shoe.url} alt={shoe.model_name} className="img-thumbnail" height="200" width="200"/> </td>
                                <td> <button className="btn btn-danger" onClick={() => handleDelete(shoe.href)}>X</button> </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
