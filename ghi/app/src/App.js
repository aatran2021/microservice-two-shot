import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import ShoesList from './ShoeList';
import ShoeForm from './ShoeForm';
import Nav from './Nav';
import HatsList from './HatsList'
import HatForm from './HatForm'
function App(props) {

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="shoes">
            <Route path="" element={<ShoesList />} />
            <Route path="new" element={<ShoeForm />} />
          </Route>
          <Route path ="hats">
            <Route path ="" element={<HatsList />} />
            <Route path ="new" element={<HatForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
